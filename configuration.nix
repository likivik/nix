# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }: {

  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];
    
  ########################################
  # BOOTLOADER
  ########################################
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.enable = true;
  # Latest Kernel
  #boot.kernelPackages = pkgs.linuxPackages_latest;
  ########################################
  # ZFS                                  #
  ########################################
  boot.supportedFilesystems = [ "zfs" ];
  networking.hostId = "30eb7afa"
  services.zfs.autoScrub.enable = true;
  ########################################
  # HARDWARE                             #
  ########################################
  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  ########################################
  # GRAPHICS                             #
  ########################################
  # Nvidia Bumblebee
  hardware.bumblebee.enable = true;
  hardware.bumblebee.driver = "nvidia";
  boot.blacklistedKernelModules = [ "nouveau" "nvidiafb" ];
  # Nvidia official drivers
  # muxless/non-MXM Optimus cards have no display outputs 
  # and show as 3D Controller in lspci output, seen in most modern consumer laptops
  # disable card with bbswitch by default since we turn it on only on demand!
  #hardware.nvidiaOptimus.disable = true;
  # install nvidia drivers in addition to intel one
  #hardware.opengl.extraPackages = [ pkgs.linuxPackages.nvidia_x11_legacy390.out ];
  #hardware.opengl.extraPackages32 = [ pkgs.linuxPackages.nvidia_x11_legacy390.lib32 ];
  # Disable Nouveau
  #boot.kernelParams = [ "nouveau.modeset=0" ];
  
  ########################################
  # NETWORKING
  ########################################
  networking.hostName = "nixos"; # Define your hostname.
  networking.networkmanager.enable = true; # remember to add users to the networkmanager group later on
  networking.networkmanager.dhcp = "dhclient"; # "dhclient" "dhcpcd" "internal"
  # Enable the OpenSSH server.
  services.sshd.enable = true;
  
  ########################################
  # LOCALE
  ########################################
  # Select internationalisation properties.
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_DK.UTF-8";
  };
  # Set your time zone.
  time.timeZone = "Europe/Moscow";
  
  ########################################
  # PACKAGES                             #
  ########################################
  nixpkgs.config = {
    allowBroken = true;
    android_sdk.accept_license = true;
    allowUnfree = true;
    firefox.enablePlasmaBrowserIntegration = true;
  };
  # Allow 32bit packages and audio
  hardware.opengl.driSupport32Bit = true;
  hardware.pulseaudio.support32Bit = true;
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment = {
    systemPackages = with pkgs; [
      #System
      htop
      #Development
      git
      wget
      neovim
      #Terminal tools
      bash
      file
      tree
      which
      pciutils
      #Filesystem tools
      gparted
      btrfs-progs
      ntfs3g
      #Compression
      unzip #unzip -P PasswOrd filename.zip
      unrar
      unar
      xarchiver
      #Video
      gnome-mpv
      #PDF
      mupdf
      #Browser
      firefox
      shadowfox
      #Networking
      networkmanager
      networkmanager_openvpn
      openvpn
      # Games
      steam
      steam-run
      glxinfo
      primus
      mesa
      (steam.override { withPrimus = true; extraPkgs = pkgs: [ bumblebee glxinfo ]; nativeOnly = true; }).run
    ];
  };
  
  ########################################
  # PRINTERS AND SCANNERS                #
  ########################################
  # Enable CUPS to print documents.
  services.printing.enable = true;
  
  ########################################
  # X11 / X.org                          #
  ########################################
  # Enable the X11 windowing system.  
  services.xserver = {
    enable = true;
    # Enable login manager
    displayManager.lightdm.enable = true;
    # Enable desktop environments
    desktopManager.plasma5.enable = true;
    windowManager.bspwm.enable = true;
    # TOUCHPAD
   
    libinput = {
      # enable touchpad support
      enable = true;
      # diable drag lock option 
      # Relevant links: 
      # https://wayland.freedesktop.org/libinput/doc/latest/tapping.html#tap-and-drag (official documentation)
      # https://wiki.archlinux.org/index.php/Libinput#Via_xinput (howto figure out commands)
      # https://nixos.org/nixos/options.html#libinput (example of additionalOptions)
      #additionalOptions = '' Option "libinput Tapping Drag Lock Enabled" 0 '';
    };
  };

  ########################################
  # USERS
  ########################################
  ### Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.likivik = {
    isNormalUser = true;
    home = "/home/likivik";
    # wheel to enable ‘sudo’ for the user, networkmanager to manage networks.
    extraGroups = [ "wheel" "networkmanager" ]; 
  }; 
  users.users.likivik.packages = with pkgs; [
    wget git kate bspwm sxhkd polybar qbittorrent
  ];
  
  ### Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.kubometr = {
    isNormalUser = true;
    home = "/home/kubometr";
    # wheel to enable ‘sudo’ for the user, networkmanager to manage networks.
    extraGroups = [ "wheel" "networkmanager" ]; 
  };
  users.users.kubometr.packages = with pkgs; [
    kate libreoffice-fresh gnome3.evolution
  ];
  
  ########################################

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.03"; # Did you read the comment?

}
