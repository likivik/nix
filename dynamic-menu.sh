#!/usr/bin/env sh 

# Modified from https://stackoverflow.com/questions/44862384/dynamically-generated-bash-menu
$ cat tst.sh
mapfile -t ifaces < <(printf 'foo\nbar code\nstuff\nnonsense\n')
for i in "${!ifaces[@]}"; do
    printf "%s) %s\n" "$i" "${ifaces[$i]}"
done
printf 'Select an interface from the above list: '
IFS= read -r opt
if [[ $opt =~ ^[0-9]+$ ]] && (( (opt >= 0) && (opt <= "${#ifaces[@]}") )); then
    printf 'good\n'
else
    printf 'bad\n'
fi