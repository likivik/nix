#!/usr/bin/env sh 

# Always use the by-id aliases for devices, otherwise ZFS can choke on imports.
# DISK=/dev/disk/by-id/...

##################################
# Partitioning wit Parted        #
##################################
# Select a disk (interactively)
HOOOW?
parted $DISK
parted mklabel gpt

# Partition 2 will be the boot partition, needed for legacy (BIOS) boot
#sgdisk -a1 -n2:34:2047 -t2:EF02 $DISK
# If you need EFI support, make an EFI partition:
#sgdisk -n1:1M:+512M -t1:EF00 $DISK
# Partition 1 will be the main ZFS partition, using up the remaining space on the drive.
#sgdisk -n2:0:0 -t2:BF01 $DISK

# Create the pool. If you want to tweak this a bit and you're feeling adventurous, you
# might try adding one or more of the following additional options:
# To disable writing access times:
#   -O atime=off
# To enable filesystem compression:
#   -O compression=lz4
# To enable normalizing unicode filenames (and implicitly set utf8only=on):
#   -O normalization=formD
# To improve performance of certain extended attributes:
#   -O xattr=sa
# For systemd-journald posixacls are required
#   -O  acltype=posixacl 
# To specify that your drive uses 4K sectors instead of relying on the size reported
# by the hardware (note small 'o'):
#   -o ashift=12
#
# The 'mountpoint=none' option disables ZFS's automount machinery; we'll use the
# normal fstab-based mounting machinery in Linux.
# '-R /mnt' is not a persistent property of the FS, it'll just be used while we're installing.
zpool create -O mountpoint=none -R /mnt rpool $DISK-part1

# Create the filesystems. This layout is designed so that /home is separate from the root
# filesystem, as you'll likely want to snapshot it differently for backup purposes. It also
# makes a "nixos" filesystem underneath the root, to support installing multiple OSes if
# that's something you choose to do in future.
zfs create -o mountpoint=none rpool/root
zfs create -o mountpoint=legacy rpool/root/nixos
zfs create -o mountpoint=legacy rpool/home

# Mount the filesystems manually. The nixos installer will detect these mountpoints
# and save them to /mnt/nixos/hardware-configuration.nix during the install process.
mount -t zfs rpool/root/nixos /mnt
mkdir /mnt/home
mount -t zfs rpool/home /mnt/home

# If you need to boot EFI, you'll need to set up /boot as a non-ZFS partition.
mkfs.vfat $DISK-part3
mkdir /mnt/boot
mount $DISK-part3 /mnt/boot

# Generate the NixOS configuration, as per the NixOS manual.
nixos-generate-config --root /mnt

# Continue with installation!
nixos-install