 1 #!/usr/bin/env sh 
 2
 3 printf "### Initiuate the Partitioning Procedure\n"
 4
 5 #parted -l
 6  
 7 #printf "Select disk to format to GPT \n"  
 8 #printf "Create Btrfs and UEFI partition \n"
 9 #printf "on Btrfs create  ROOT (/), HOME (/home), and SWAP(/swapfile) \n"
10  
11  
12 available_disks=( $( parted -l | grep "Disk /" | grep "/" ) )
13 declare -p available_disks
14 
15 select disk in $available_disks;
16 do
17         echo "Your choice: ${disk}"
18         break
19 done
20 
